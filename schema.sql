CREATE TABLE users (
    id     serial PRIMARY KEY,
    login  varchar(20) UNIQUE NOT NULL,
    password varchar(255) NOT NULL,
    token varchar(255) UNIQUE DEFAULT NULL
);

INSERT INTO users (login, password) VALUES ('user1', '$2a$10$uaBFPdBqnU2V5mrOEwwvK./GZXaE1xjHESHvoRIWktdmkYtJ9YRhq');

