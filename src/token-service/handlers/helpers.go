package handlers

import (
	"encoding/json"
	"net/http"
)

type errorResponse struct {
	Text string `json:"error"`
}

func HttpErrorResponse(responseWriter http.ResponseWriter, errorCode int, errorText string) {

	error_response, _ := json.Marshal(errorResponse{Text: errorText})

	http.Error(
		responseWriter,
		string(error_response),
		errorCode,
	)

	return
}
