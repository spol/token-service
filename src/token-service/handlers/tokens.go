package handlers

import (
	"encoding/json"
	"net/http"
	"token-service/services"
)

type Tokens struct {
	service *services.TokenIssue
}

func NewTokens(service *services.TokenIssue) *Tokens {
	return &Tokens{service: service}
}

type tokenRequestParams struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type TokenResponse struct {
	Token string `json:"token"`
}

func (tokens *Tokens) ServeHTTP(responseWriter http.ResponseWriter, request *http.Request) {
	if request.Method != http.MethodPost {

		HttpErrorResponse(
			responseWriter,
			http.StatusMethodNotAllowed,
			http.StatusText(http.StatusMethodNotAllowed),
		)

		return
	}

	tokenRequestParams, err := decodeTokenRequestParams(request)

	if err != nil {

		HttpErrorResponse(
			responseWriter,
			http.StatusBadRequest,
			err.Error(),
		)

		return
	}

	token, err := tokens.service.Call(tokenRequestParams.Login, tokenRequestParams.Password)

	if err != nil {
		HttpErrorResponse(
			responseWriter,
			http.StatusBadRequest,
			err.Error(),
		)
		return
	}

	response, _ := json.Marshal(TokenResponse{Token: token})

	responseWriter.WriteHeader(http.StatusAccepted)
	responseWriter.Write(response)

}

func decodeTokenRequestParams(request *http.Request) (*tokenRequestParams, error) {
	rp := &tokenRequestParams{}
	decoder := json.NewDecoder(request.Body)

	if err := decoder.Decode(rp); err != nil {
		return nil, err
	}

	return rp, nil
}
