package handlers

import (
	"encoding/json"
	"errors"
	"net/http"
	"token-service/services"
)

const (
	TOKEN_HEADER_NAME = "X-TS-AUTH"
)

type Users struct {
	service *services.UserLogin
}

func NewUsers(service *services.UserLogin) *Users {
	return &Users{service: service}
}

type userLoginResponse struct {
	Login string `json:"token"`
}

func (users *Users) ServeHTTP(responseWriter http.ResponseWriter, request *http.Request) {

	if request.Method != http.MethodGet {

		HttpErrorResponse(
			responseWriter,
			http.StatusMethodNotAllowed,
			http.StatusText(http.StatusMethodNotAllowed),
		)

		return
	}

	token, err := fetchToken(request)

	if err != nil {
		HttpErrorResponse(
			responseWriter,
			http.StatusBadRequest,
			err.Error(),
		)
		return
	}

	login, err := users.service.Call(token)

	if err != nil {
		HttpErrorResponse(
			responseWriter,
			http.StatusNotFound,
			http.StatusText(http.StatusNotFound),
		)
		return
	}

	response, _ := json.Marshal(userLoginResponse{Login: login})

	responseWriter.Write(response)

}

func fetchToken(request *http.Request) (string, error) {
	t := request.Header.Get(TOKEN_HEADER_NAME)

	if t == "" {
		return t, errors.New("Missing token")
	}

	return t, nil
}
