package utils

import (
	"crypto/sha256"
	"encoding/base64"
	"time"
)

type TokenGenerator interface {
	GenerateToken(string) string
}

type TokenUtil struct {
	secret string
}

func NewTokenUtil(secret string) *TokenUtil {
	return &TokenUtil{secret}
}

func (t *TokenUtil) GenerateToken(source string) string {

	h := sha256.New()

	h.Write([]byte(t.secret))
	h.Write([]byte(getIssueStringTime()))
	h.Write([]byte(source))

	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}

func getIssueStringTime() string {
	return time.Now().String()
}
