package main

import (
	"net/http"
	"token-service/handlers"
	"token-service/models"
	"token-service/services"
	"token-service/utils"
	"database/sql"
)

type Application struct {
	database *sql.DB
	tokensHandler *handlers.Tokens
	usersHandler  *handlers.Users
}

func NewApplication() *Application {

	database := models.NewDB("user=ubuntu password=123 dbname=token_service")
	tokenGenerator := utils.NewTokenUtil("supersecret")
	usersRepository := models.NewUsersRepository(database)
	tokensIssue := services.NewTokenIssue(usersRepository, tokenGenerator)
	userLogin := services.NewUserLogin(usersRepository)

	return &Application{
		database: database,
		tokensHandler: handlers.NewTokens(tokensIssue),
		usersHandler:  handlers.NewUsers(userLogin),
	}
}

func NewRouter(app *Application) http.Handler {
	router := http.NewServeMux()

	router.Handle("/tokens", app.tokensHandler)
	router.Handle("/users", app.usersHandler)

	return router
}

func main() {
	app := NewApplication()

	http.Handle("/tokens", app.tokensHandler)
	http.ListenAndServe(":3000", NewRouter(app))
}
