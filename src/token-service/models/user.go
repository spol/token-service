package models

import (
	"golang.org/x/crypto/bcrypt"
	"token-service/utils"
)

type User struct {
	Id       int
	Login    string
	Password string
	Token    string
}

func (user *User) IsPasswordValid(password string) bool {
	p1, p2 := []byte(user.Password), []byte(password)

	if err := bcrypt.CompareHashAndPassword(p1, p2); err != nil {
		return false
	}
	return true
}

func (user *User) GenerateToken(tokenGenerator utils.TokenGenerator) error {
	user.Token = tokenGenerator.GenerateToken(user.Login)
	return nil
}
