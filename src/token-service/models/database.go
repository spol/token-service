package models

import (
	"database/sql"
	_ "github.com/lib/pq"
	"log"
)

func NewDB(dbstring string) *sql.DB {

	db, err := sql.Open("postgres", dbstring)

	if err != nil {
		log.Fatal(err)
	}

	if err = db.Ping(); err != nil {
		log.Fatal(err)
	}

	return db
}
