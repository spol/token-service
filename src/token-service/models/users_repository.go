package models

import (
	"database/sql"
	_ "github.com/lib/pq"
)

type ErrNotFound struct {
	message string
}

func NewErrNotFound(message string) *ErrNotFound {
	return &ErrNotFound{
		message: message,
	}
}

func (e *ErrNotFound) Error() string {
	return e.message
}

////////////////////////////////////////////////////////////////////
type UsersRepository struct {
	database *sql.DB
}

func NewUsersRepository(db *sql.DB) *UsersRepository {
	return &UsersRepository{database: db}
}

////////////////////////////////////////////////////////////////////
func (userRepo *UsersRepository) Update(user *User) (bool, error) {
	result, err := userRepo.prepareAndExecuteSQL("UPDATE users SET token = $1 WHERE users.id = $2", user.Token, user.Id)
	return result == 1, err
}

func (userRepo *UsersRepository) FindByLogin(login string) (*User, error) {
	user, err := userRepo.prepareAndQueryRow("SELECT users.* FROM users WHERE users.login = $1", login)
	return user, err
}

func (userRepo *UsersRepository) FindByToken(token string) (*User, error) {
	user, err := userRepo.prepareAndQueryRow("SELECT users.* FROM users WHERE users.token = $1", token)
	return user, err
}

//////////////////////////////////
func (userRepo *UsersRepository) prepareAndQueryRow(SQL string, values ...interface{}) (*User, error) {

	user := User{}

	stmt, err := userRepo.database.Prepare(SQL)

	if err != nil {
		panic(err)
		return &user, err
	}

	var id int
	var login string
	var password string
	var token sql.NullString

	err = stmt.QueryRow(values...).Scan(&id, &login, &password, &token)

	if err != nil {
		if err == sql.ErrNoRows {
			return &user, NewErrNotFound("Not found")
		}
		panic(err)
	}

	user.Id = id
	user.Login = login
	user.Password = password

	if token.Valid {
		user.Token = token.String
	} else {
		user.Token = ""
	}

	return &user, nil
}

func (userRepo *UsersRepository) prepareAndExecuteSQL(SQL string, values ...interface{}) (int64, error) {
	stmt, err := userRepo.database.Prepare(SQL)

	if err != nil {
		panic(err)
	}
	res, err := stmt.Exec(values...)

	if err != nil {
		panic(err)
	}

	result, _ := res.RowsAffected()

	return result, nil
}
