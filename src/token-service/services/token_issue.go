package services

import (
	"token-service/models"
	"token-service/utils"
)

type ErrInvalidCredentials struct {
	message string
}

func NewErrInvalidCredentials(message string) *ErrInvalidCredentials {
	return &ErrInvalidCredentials{
		message: message,
	}
}

func (e *ErrInvalidCredentials) Error() string {
	return e.message
}

////////////////////////////////////////////////////////////////////
type TokenIssue struct {
	userRepository *models.UsersRepository
	tokenGenerator utils.TokenGenerator
}

func NewTokenIssue(userRepo *models.UsersRepository, tokenGenerator utils.TokenGenerator) *TokenIssue {
	return &TokenIssue{userRepository: userRepo, tokenGenerator: tokenGenerator}
}

////////////////////////////////////////////////////////////////////
func (service *TokenIssue) Call(login string, password string) (string, error) {

	user, err := service.userRepository.FindByLogin(login)

	if err != nil {
		if _, ok := err.(*models.ErrNotFound); ok {
			return "", NewErrInvalidCredentials("Wrong user credentials")
		}
		return "", err
	}

	if user.IsPasswordValid(password) {
		user.GenerateToken(service.tokenGenerator)

		_, err := service.userRepository.Update(user)

		if err != nil {
			return "", err
		}

		return user.Token, nil

	} else {
		return "", NewErrInvalidCredentials("Wrong user credentials")
	}

}
