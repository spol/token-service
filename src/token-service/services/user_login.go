package services

import (
	"token-service/models"
)

type ErrNotFound struct {
	message string
}

func NewErrNotFound(message string) *ErrNotFound {
	return &ErrNotFound{
		message: message,
	}
}

func (e *ErrNotFound) Error() string {
	return e.message
}

////////////////////////////////////////////////////////////////////
type UserLogin struct {
	userRepository *models.UsersRepository
}

func NewUserLogin(userRepo *models.UsersRepository) *UserLogin {
	return &UserLogin{userRepository: userRepo}
}

func (service *UserLogin) Call(token string) (string, error) {
	user, err := service.userRepository.FindByToken(token)

	if err != nil {
		if _, ok := err.(*models.ErrNotFound); ok {
			return "", NewErrNotFound("Not Found")
		}
		return "", err
	}

	return user.Login, nil
}
