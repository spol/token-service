package main

import (
	"github.com/DATA-DOG/godog"
	"net/http/httptest"
	"net/http"
	"token-service/handlers"
	"token-service/models"
	"token-service/services"
	"token-service/utils"
	"database/sql"
	"golang.org/x/crypto/bcrypt"
	"encoding/json"
	"bytes"
	"fmt"
	"strings"
)

type apiFeature struct {
	resp *httptest.ResponseRecorder
	app *Application
	router http.Handler
	database *sql.DB
}

func setupApiFeature() *apiFeature{
	database := models.NewDB("user=ubuntu password=123 dbname=token_service_test")
	tokenGenerator := utils.NewTokenUtil("supersecret")
	usersRepository := models.NewUsersRepository(database)
	tokensIssue := services.NewTokenIssue(usersRepository, tokenGenerator)
	userLogin := services.NewUserLogin(usersRepository)

	app := &Application{
		database: database,
		tokensHandler: handlers.NewTokens(tokensIssue),
		usersHandler:  handlers.NewUsers(userLogin),
	}

	router := NewRouter(app)


	return &apiFeature{app: app, router: router, database: database}
}

func (a *apiFeature) resetResponse(interface{}) {
	a.resp = httptest.NewRecorder()
}

func (a *apiFeature) clearUsersTable(interface{}) {
	_, err := a.database.Exec("DELETE FROM users")
	if err != nil {
			panic(err)
	}
	return
}

func (a *apiFeature) createUser(login string, password string, token sql.NullString) error{

	encrypted_password, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	_, err := a.database.Exec("INSERT INTO users (login, password, token) VALUES ($1, $2, $3);", login, encrypted_password, token)
	if err != nil {
			return err
	}
	return nil
}

func (a *apiFeature)thereIsAUserWithPassword(login, password string) error {
	a.createUser(login, password, sql.NullString{String:"", Valid:false})
	return nil
}

func (a *apiFeature)iIssueATokenAsWithPassword(login, password string) error {

	payload := make(map[string]string)

	payload["login"] = login
	payload["password"] = password

	jsonPayload,_ := json.Marshal(payload)

	req, err := http.NewRequest("POST", "/tokens", bytes.NewBuffer(jsonPayload))

  if err != nil {
      return err
  }

  a.router.ServeHTTP(a.resp, req)

  return nil
}

func (a *apiFeature)iSeeResponseWithValidTokenWithCode(code int) error {

	responseBody := a.resp.Body.String()

	if code != a.resp.Code{
			return fmt.Errorf("expected response code to be: %d, but actual is: %d", code, a.resp.Code)
	}

	if !strings.Contains(responseBody, "token") {
		return fmt.Errorf("expected response to contains valid token")
	}

	return nil
}

func (a *apiFeature)iSeeResponseWithCode(text string, code int) error {
	responseBody := a.resp.Body.String()

	if !strings.Contains(responseBody, text) {
		return fmt.Errorf("expected response to contains text %s, got %s", text, responseBody)
	}

	if code != a.resp.Code{
			return fmt.Errorf("expected response code to be: %d, but actual is: %d", code, a.resp.Code)
	}
	return nil
}

func (a *apiFeature)thereIsAUserWithToken(login string, token string) error {
	a.createUser(login, "password", sql.NullString{String: token, Valid: true})
	return nil
}

func (a *apiFeature)iAskForUserLoginWithToken(token string) error {
	req, err := http.NewRequest("GET", "/users", nil)
  if err != nil {
      return err
  }

  req.Header.Set("X-TS-AUTH", token)

  a.router.ServeHTTP(a.resp, req)

  return nil
}




func FeatureContext(s *godog.Suite) {

	api := setupApiFeature()

	s.BeforeScenario(api.resetResponse)
	s.BeforeScenario(api.clearUsersTable)


	s.Step(`^there is a user "([^"]*)" with password "([^"]*)"$`, api.thereIsAUserWithPassword)
	s.Step(`^I issue a token as "([^"]*)" with password "([^"]*)"$`, api.iIssueATokenAsWithPassword)
	s.Step(`^I see response with valid token with code (\d+)$`, api.iSeeResponseWithValidTokenWithCode)
	s.Step(`^I see response "([^"]*)" with code (\d+)$`, api.iSeeResponseWithCode)
	s.Step(`^there is a user "([^"]*)" with token "([^"]*)"$`, api.thereIsAUserWithToken)
	s.Step(`^I ask for user login with token "([^"]*)"$`, api.iAskForUserLoginWithToken)
}
