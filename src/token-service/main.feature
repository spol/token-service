Feature: token service usage

	Scenario: Issue a token with correct credentials
		Given there is a user "John" with password "12345"
		When I issue a token as "John" with password "12345"
		Then I see response with valid token with code 202

	Scenario: Issue a token with wrong credentials
		Given there is a user "John" with password "12345"
		When I issue a token as "John" with password "12346"
		Then I see response "Wrong user credentials" with code 400

	Scenario: Get user login by token
		Given there is a user "John" with token "6b9b9319"
		When I ask for user login with token "6b9b9319"
		Then I see response "John" with code 200

	Scenario: Get user login by unexisting token
		Given there is a user "John" with token "6b9b9319"
		When I ask for user login with token "7b9b9319"
		Then I see response "Not Found" with code 404
